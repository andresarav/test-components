import React, { useEffect, useState } from 'react'
import DropDownComponent from './dropDownFilter'
import { Container } from './dropDownFilter/styles'


const SelectFilter = props => {

  const [ select ] = useState([ {name:"Depósitos", param:"deposits"}, {name:"Retiros", param:"withdraws"} ])
  const [ item_active ] = useState({ withdraws:true })


  useEffect(()=>{
    if(!select){
      // props.action.create_select_list({path, filter_name})
    }
  }, [select])
 

  const actionHandle = (e) => {
    console.log('==========================> actionHandle', e)
  }

  return(
    <Container>
      { 
        (select) ?
          <DropDownComponent
            data={select}
            select_state={item_active}
            actionHandle={actionHandle}
          />
          :
          <p>
            Cargando...
          </p>
      }
    </Container>
  )

}




export default SelectFilter
