import React, { useState, useEffect, useRef } from 'react'

import {
  DropDown,
  DropDownList
} from './styles'

import { MdArrowDropDown, MdArrowDropUp } from 'react-icons/md'
// import { modal_action } from '../../../../actions/ui'

 
const DropDownComponent = (props) => {

  // console.log('==========================> DropDownComponent', props)
  // debugger
  let isSuscribe = useRef(true);
  // let isSuscribe = true

  const [ filterSwitch, setFilterSwitch ] = useState(false)
  const [ currentFilter, setCurrentFilter ] = useState(props.data[0])

  useEffect(() => {
      get_active_item(props.select_state)
      window.addEventListener('click', (e => eventHandle(e)))
      return () =>{
        window.removeEventListener('click', e => eventHandle(e))
        isSuscribe.current = false
      }
  }, [props.select_state])


  const get_active_item = async(state) => {
    if(!state){return false}
    let query

    await Object.keys(state).forEach(item => {
     if(state[item]){
       query = item
       // console.log('=================================>>>>>>>>>>>>>>>>>>>>>> get_active_item', item)
     }
   })

       console.log('=================================>>>>>>>>>>>>>>>>>>>>>> ', query, props.data)
      //  debugger


   if(!query){return false}
     props.data.filter((item)=>{
       return (item.param.toLowerCase().includes(query) && isSuscribe.current) && setCurrentFilter(item)
     })
  }



  const eventHandle = (e) => {
    // console.log('eventHandle', e.target.dataset)
    if(!e.target.dataset.drop_down){
      // console.log(filterSwitch)
      setFilterSwitch(false)
    }

  }

  const toggleFilter = () => {
    setFilterSwitch(!filterSwitch)
  }

  const changeFilter = e => {
    // console.log('___changeFilter', e.target.dataset)
    const { filter_name, filter_param } = e.target.dataset

    let payload = {
      name:filter_name,
      param:filter_param
    }

    setCurrentFilter(payload)
    props.actionHandle(payload, props)
  }

  let IconDropDown = filterSwitch ? MdArrowDropUp : MdArrowDropDown
  return(
      <DropDown data-drop_down className="DropDown" width="100%" height="40px">
        <span data-drop_down className="currentFilter" onClick={toggleFilter}>
          <p data-drop_down>{currentFilter && currentFilter.name}</p>
          <IconDropDown data-drop_down/>
        </span>
        {
          filterSwitch &&
          <DropDownList data-drop_down className="DropDownList">
            {
            props.data.map((filter, key) => {
                return <li data-filter_name={filter.name} data-filter_param={filter.param} key={key} className="itemFilter" onClick={changeFilter}>{filter.name}</li>
              })
            }
          </DropDownList>
        }

      </DropDown>

  )

}


export default DropDownComponent
