import styled from 'styled-components'



export const Container = styled.div`
  max-width:250px;
`

export const DropDownList = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  position: absolute;
  width: 100%;
  height: auto;
  top: 38px;
  background: white;
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
  background: #ececec;
  z-index: 5;

  .itemFilter{
    margin: 20px 15px;
    font-size: 14px;
    color: gray;
    cursor: pointer;
  }
  .itemFilter:hover{
    color: #1ea4ff;
  }
`

export const DropDown = styled.div`
  width: ${props => props.width ? props.width : "140px"};
  height: ${props => props.height ? props.height : "auto"};
  display: grid;
  align-items: center;
  border-right: 1px solid #d8d8d8;
  position: relative;
  background: #ececec;
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;

  .currentFilter{
    display: grid;
    grid-template-columns: 1fr 25px;
    align-items: flex-end;
    grid-column-gap: 10px;
    margin-left: 15px;
    cursor: pointer;
    height: 100%;
    align-items: center;
  }
  .currentFilter:hover{
    color: #1ea4ff;
  }

  .currentFilter p{
    font-size: 14px;
  }
`
