import React, { useEffect } from 'react'

export default () => {

  // en este ejemplo vamos a utilizar templates html dentro de un json para el manejo de multiples lenguajes
  let lang = 'en' //extraeríamos el lenguaje de la url
  let translation = {
    en:{
      title:`Hola chiquita`
    }
  }

  useEffect(()=>{

  }, [])

  const { title } = translation[lang]

  return(
    <div id="demo">
      {
        document.getElementById("demo").innerHTML = title
      }
    </div>
  )

}



//
