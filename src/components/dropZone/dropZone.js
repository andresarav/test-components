import React, {useCallback} from 'react'
import { useDropzone } from 'react-dropzone'
import styled from 'styled-components'

function MyDropzone() {

  const onDrop = useCallback(acceptedFiles => {
    // Do something with the files
  }, [])

  const { getRootProps, getInputProps, isDragActive } = useDropzone({onDrop})
  console.log(isDragActive)

  return (
    <Container {...getRootProps()}>
      <input {...getInputProps()} />
      {
        isDragActive ?
          <p>Drop the files here ...</p> :
          <p>Drag 'n' drop some files here, or click to select files</p>
      }
    </Container>
  )
}

export default MyDropzone


const Container = styled.div`
  width: 80%;
  height: 40vh;
  background: gray;

`
