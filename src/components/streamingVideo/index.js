import React, { useEffect, useRef } from 'react'

export default () => {

  const videoEl = useRef(null);
  const canvasEl = useRef(null);
  const faceApi = useRef(window.faceapi)

  const handleVideo = vid => {
    // console.log('video => ', vid)
    videoEl.current.srcObject = vid;
  }

  const videoError = err => {
    console.log(err) 
  }

  const init = () => {
    const canvas = canvasEl.current
    const video = videoEl.current
    const ctx = canvasEl.current.getContext('2d')
    // setInterval(() => {
      // emitFrame(ctx, video, canvas)
    // }, 500)
  }

  const emitFrame = (ctx, video, canvas) => {
    ctx.drawImage(video, 0,0)
    let dataURL = canvas.toDataURL('image/jpeg')
    console.log('||||||||||| Snapshot image ==> ', dataURL)
  }

  const initFaceApi = async() => {
    const detections = await faceApi.current.detectAllFaces(videoEl.current)
    console.log(detections)
  } 
  

  useEffect(()=>{
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
    if (navigator.getUserMedia) {       
      navigator.getUserMedia({video: true}, handleVideo, videoError);
      // init()
      initFaceApi()
    }
  }, [])

  return(
    <>
      {/* <video autoPlay={true} ref={videoEl} controls/> */}
      {/* <video style={{display:'none'}} autoPlay={true} ref={videoEl} controls/> */}
      <canvas ref={canvasEl} width={700} height={600}/>
    </>
  )

}

