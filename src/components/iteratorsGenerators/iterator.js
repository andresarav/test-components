import React from 'react'
import styled from 'styled-components'

export default () => {

  const myArray = new Array(10).fill('a')

  const crearIterator = () => {
    let nextIndex = 0
    return {
      next:() => {
        return nextIndex < myArray.length ?
        {value:myArray[nextIndex++], index:nextIndex, done:false} : {done:true}
      }
    }
  }

  const iterator = crearIterator()

  return (
    <div>
      <Click onClick={() => {console.log(iterator.next())}}>Clickeame</Click>
      Iterator component
    </div>
  )

}


const Click = styled.p`
  width: 100px;
  background: red;
  cursor: pointer;
`
