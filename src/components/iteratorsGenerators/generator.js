import React from 'react'
import styled from 'styled-components'

export default () => {

  const myArray = new Array(10).fill('a')

  function* counterGenerator() {
    let i = 0
    while (i < myArray.length) {
      yield i
      i++
    }
  }

  const counter = counterGenerator()

  return (
    <div>
      Generator component
      <Click onClick={() => {console.log(counter.next())}}>Clickeame</Click>
    </div>
  )

}


const Click = styled.p`
  width: 100px;
  background: red;
  cursor: pointer;
`
