import React, {useEffect} from 'react'

export default () => {

  function* oddsGenerator() {
    let n = 0
    while (true) {
      yield 2*n + 1
      n++
    }
  }

  function take(n, iter) {
    let counter = n
    for (let c of iter) {
      console.log(c)
      counter--
      if(counter <= 0) break
    }
  }

  var oddNumbers = oddsGenerator() // TODOS los números impares
  take(10, oddNumbers)

  return null

}
  


export const Minified = () => {

  function* oddsGenerator(limit) {
    let n = 0
    while (n < limit) {
      yield 2*n + 1
      n++
    }
  }

  useEffect(()=>{
    console.log([...oddsGenerator(10)])
  }, [])

  return null

}


// Esto es lo que se llama evaluación perezosa y es un concepto importante en lenguajes funcionales como Haskell. Básicamente nos permite tener
// listas o estructuras de datos “infinitas” y operar sobre ellas, por ejemplo podemos tener un operador take(n) que toma los N primeros elementos
// de una lista infinita.


// La evaluación perezosa permite construir este tipo de estructuras “infinitas” o completas sin producir errores de ejecución y también son más eficientes
 // en algoritmos de búsqueda, recorrido de árboles y cosas así, al evaluar el mínimo número de nodos necesarios para encontrar la solución.
