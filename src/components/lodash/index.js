import React, {useEffect} from 'react'
import _ from 'lodash';

const personas = [
    {
        nombre: "pepe",
        edad: 20
    },
    {
        nombre: "andres aguilar",
        edad: 20
    },
    {
        nombre: "ana",
        edad: 30
    },
    {
        nombre: "pedro",
        edad: 40
    },
    {
        nombre: "gema",
        edad: 50
    },
    {
        nombre: "andres",
        edad: 50
    }
];

const Lodash = () => {

  // const filter = _.filter(personas, function(persona) {
  //   return persona.edad === 50
  // });

  // var primerMayor= _.find(personas,function(persona) {
  //     return persona.edad>18;
  // });

  // console.log(_.join(['a', 'b', 'c'], ' ~ '));
  // => 'a~b~c'

  // Crea una matriz de elementos divididos en grupos de longitud size. Si arrayno se puede dividir en partes iguales, la porción final serán los elementos restantes.
  // console.log(_.chunk(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'], 3));
  // => [['a', 'b', 'c'], ['d', 'e', 'f'], ['g', 'h', 'i']]

  // Creates an array with all falsey values removed. The values false, null, 0, "", undefined, and NaN are falsey.
  // console.log(_.compact([0, 1, false, 2, '', 3, null]));

  // console.log(_.difference([1, 2, 3, 4, 5, 6, 7, 8, 9], [2, 3, 6]));
  // => [1, 4, 5, 7, 8, 9]


  // Crea un arreglo excluyendo el objeto insertado con el comparador _.isEqual
  // var objects = [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }];
  // console.log(_.differenceWith(objects, [{ 'x': 1, 'y': 2 }], _.isEqual));
  // => [{ 'x': 2, 'y': 1 }]

  // var array = [1, [2, [3, [4]], 5]];
  // console.log(_.flattenDeep(array))
  // => [1, 2, 3, 4, 5]

  // console.log(_.flattenDepth(array, 2))
  // => [1, 2, 3, [4], 5]


  return null

}

export default Lodash
