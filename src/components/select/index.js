import React from 'react'
const banks = [
  "Banco de Bogotá",
  "Banco Popular",
  "Corpbanca",
  "Bancolombia",
  "Scotiabank Colpatria Colombia",
  "Banco GNB Colombia",
  "BBVA Colombia",
  "Itau",
  "Red Multibanca Colpatria",
  "Banco de Occidente",
  "Banco Caja Social",
  "Banco Agrario de Colombia",
  "Banco Davivienda",
  "Banco AV Villas",
  "Bancamia",
  "Banco Pichincha",
  "Bancoomeva",
  "Banco Falabella",
  "Banco Finandina",
  "Banco Santander",
  "Banco Nequi",
  "Banco W"
]

const SelectComponent = props => {

  return(
    <div>
      <form action="/action_page.php">
      <option value={-1} disabled>Seleccione un banco</option>

        <select name="cars" id="cars">
          {
            banks.map(bank => <option key={bank}>{bank}</option>)
          }
        </select>
      </form>
    </div>
  )

}

export default SelectComponent
