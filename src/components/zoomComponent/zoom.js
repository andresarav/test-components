import React from 'react'
// import ReactImageMagnify from 'react-image-magnify';
import ReactImageZoom from 'react-image-zoom';
import mia from './mia.jpg'
import styled from 'styled-components'

const ZoomComponent = () => {

const props = {width: 100, height: 100, img: mia, zoomPosition:'top', offset:{"vertical":100, "horizontal":200}};

  return(
    <Section>
      <ContainerImgSrc>
        <ReactImageZoom {...props} />
      </ContainerImgSrc>
      <ImgResult>

      </ImgResult>
    </Section>
  )

}

export default ZoomComponent


const ImgResult = styled.div`
  background: gray;
`

const Section = styled.section`

  display: grid;
  grid-template-columns: auto auto;
  height: 100vh;
  align-items: center;
  justify-items:center;

`


const ContainerImgSrc = styled.div`
  background: gray;
  height: 110px;
  width: 110px;
  display: grid;
  align-items: center;
  justify-content: center;
`
