import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { useObserver } from '../../hooks/useObserver'

const InifiniteScroll = () => {

  const [ items, setItems ] = useState(new Array(13).fill(0).map((_val, i) => i + 1))
  const [ loader, setLoader ] = useState()
  const [ show, setElement ] = useObserver()

  const loadMoreItems = () => {
    const MOREITEMS = new Array(5).fill(0).map((_val, i) => ((i + items.length)  + 1))
    setItems([...items, ...MOREITEMS])
  }

  useEffect(()=>{
    if(show){
      setLoader(true)
      setTimeout(()=>{
        loadMoreItems()
        setLoader(false)
      }, 1000)
    }
  }, [show])

  return (
    <Container>
      <ItemsContainer>
        {
          items.map((item, indx)=>{
            return <p key={indx} >
              {item}
            </p>
          })
        }
      </ItemsContainer>
      {
        !loader ?
        <CtaMore id="loadMore" onClick={loadMoreItems} ref={setElement}>
          Cargar más...
        </CtaMore>
        : 'cargando...'
      }
    </Container>
  )

}


export default InifiniteScroll

const CtaMore = styled.div`
  width: 250px;
  height: 60px;
  border-radius: 6px;
  background: white;
  display: grid;
  align-items: center;
  justify-items:center;
  cursor: pointer;
  opacity: 0;
`

const ItemsContainer = styled.section`
  display: grid;
  height: auto;
  width: 100%;
  row-gap:15px;
  grid-template-rows: repeat(auto-fill, 90px);

  p{
    background: yellow;
    margin: 0;
    height: 90px;
    text-align: center;
    line-height: 90px;
  }
`

const Container = styled.div`
  ${'' /* width: calc(100vw - 160px); */}
  min-height: calc(100vh - 160px);
  height: auto;
  background: green;
  display: grid;
  justify-items:center;
  padding: 80px;
  grid-template-rows: 1fr 60px;
  row-gap:25px;
`
