import React, { Component } from 'react'
import './icons.css'

import {
  Bitcoin2,
  Ethereum,
  Litecoin,
  IconDefault
 } from './'

class IconSwitch extends Component {

  switcher = props => {
    const { icon } = props
    switch (icon) {
      case 'bitcoin':
      case 'bitcoin_testnet':
        return <Bitcoin2 {...props} />
      case 'ethereum':
        return <Ethereum {...props} />
      case 'litecoin':
        return <Litecoin {...props} />
      default:
        return <IconDefault {...props} />
    }
  }


  render(){

    const{
      animOn
    } = this.props

    return(
        <div className={`iconSty ${animOn ? 'animOn' : '' }`}>
          <this.switcher {...this.props} />
        </div>
    )
  }

}


export default IconSwitch
