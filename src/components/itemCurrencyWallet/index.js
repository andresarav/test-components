import React, { Fragment, useState } from 'react'
import IconSwitch from './icons/iconSwitch'
import backcard from './back.png'
import { currencies } from './currencies.json'
import './item_wallet.css'



export default (props) => {


  const [currency, setCurrency] = useState(currencies[0])


    return(
      <Fragment>
        <div className={`ItemTemplate contWalleins animate`}>

                <div
                  id="ItemWallet"
                  className={`  ItemWallet2 ${currency.red.toLowerCase()} cryptoWallet`}
                  >

                    <img src={backcard} id="backCard" alt="" width="100%" height="100%"/>
                    <div className="contIconsTem" style={{height:`${currencies.length ? currencies.length : '1'}00%`, top:`-${(currency.id-1)}00%`}} >
                        {
                          currencies.map(currencyItem => {
                            return(
                                  <div className={`iconBank2Cont ${currency.red === currencyItem.red ? 'ON' : ''}`} key={currencyItem.id}>
                                    <IconSwitch icon={currencyItem.red.toLowerCase()} size={160}/>
                                  </div>
                            )
                          })
                        }
                    </div>

                  <h1 className="IWText titu fuente tobe_continue"> Mi cartera {currency.red} </h1>

                    <p className="IWText fuente IWcurrencyText tobe_continue">
                      {currency.red}
                    </p>

                    <p className="IWText fuente IWcurrencyText tobe_continue">
                      {currency.status}
                    </p>

                </div>
              </div>
        </Fragment>
    )

}
