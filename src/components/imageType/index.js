import { useEffect } from 'react';
import imageType from 'image-type'
import styled from 'styled-components'


const ImageTypeComponent = props => {

    const IMAGE_MIME_TYPES = [
        "image/png",
        "image/jpeg",
        "image/jpg"
    ];
     
    function includesAnyImageMime(value){

        let allowed_img_mimes = IMAGE_MIME_TYPES;
    
        if (typeof(value) != "string" && !(value instanceof String)) return false;

        let image_buffer = Buffer.from(value, 'base64');
        let image_info = imageType(image_buffer);
        if(!image_info) return false;
    
        let found = allowed_img_mimes.find((allowed_img_mime) => {
          return allowed_img_mime === image_info.mime;
        });

        if (!found) return false;
    
        return image_info;
    }

    const readFile = (file) => {
        return new Promise((resolve) => {
          const reader = new FileReader();
          reader.addEventListener("load", () => resolve(reader.result), false);
          reader.readAsDataURL(file);
        });
    };


    const goFileLoader = async (e) => {
        if (e.target.files && e.target.files.length > 0) {
            const data = e.target.files[0];
            const dataBase64 = await readFile(data);
            const isAnImage = includesAnyImageMime(dataBase64.split(",")[1])
            if(!isAnImage){
                return alert('Solo se aceptan imagenes')
            }
            const img = document.querySelector('#image')
            img.src = dataBase64
        }
    };


    // useEffect(()=>{
    //     // const url = 'https://upload.wikimedia.org/wikipedia/en/a/a9/Example.jpg';

    //     // fetch(url)
    //     // .then(response => {
    //     //     response.on()
    //     // })

    //     // fetch(url, response => {
    //     //     response.on('readable', () => {
    //     //         const chunk = response.read(imageType.minimumBytes);
    //     //         response.destroy();
    //     //         console.log(imageType(chunk));
    //     //         //=> {ext: 'jpg', mime: 'image/jpeg'}
    //     //     });
    //     // });
    // }, [])

    return(
        <Container>
            <h1>IMAGE TYPE</h1>
            <img id="image" alt="Imagen sin cargar" width={400} ></img>

            <input 
                type="file"
                id="avatar" 
                name="avatar"
                accept="image/*"
                onChange={goFileLoader}
            />
        </Container>
    )

} 

export default ImageTypeComponent

const Container = styled.div`
    display: flex;
    flex-direction: column;
    img{
        min-height:200px;
        height:auto;
    }
`