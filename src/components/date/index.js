import React, { useState, useEffect, useRef } from 'react'
import BigNumber from "bignumber.js";
import moment from 'moment'
import 'moment/locale/es'

import MaskedInput from "react-text-mask";
import createAutoCorrectedDatePipe from "text-mask-addons/dist/createAutoCorrectedDatePipe";
const autoCorrectedDatePipe = createAutoCorrectedDatePipe("dd/mm/yyyy", {
  minYear:1900,
  maxYear:new Date().getFullYear() - 18
});
 
export default () => {

  const [ value, setValue ] = useState()
  
  const formatMaskDate = date => {
    const day = date.split("/")[0]
    const month = date.split("/")[1]
    const year = date.split("/")[2]
    return `${year}-${month}-${day}`
  }

  const onChange = async e => {  
    const date = formatMaskDate(e?.target?.value)
    const [ _value, status ] = birthday(date)
  }

  const birthday = (value) => {
    let birthYear = new Date(value)?.getFullYear() || new Date().getFullYear()
    let currentYear = new Date().getFullYear()
    let age = currentYear - birthYear 
    let status = (age >= 18 && age < 100) && 'success' 
    let _value = value
    return [ _value, status ]
  }
 
  const onChange2 = async(e) => {  
    const _timeStamp = new Date(`${e.target.value}T00:00:00`).getTime()
    const birthdayTimeStamp = BigNumber(_timeStamp).div(1000).toString()

    const getBirth = await formatTimeStampToLegibleDate(birthdayTimeStamp)
    console.log('||||||| getBirth ==> ', getBirth)
  }

  const formatTimeStampToLegibleDate = async(timeStamp) => {
    let date = new Date(new BigNumber(timeStamp).multipliedBy(1000).toNumber())
    let legibleDate = await moment(date).format('ll')
    return legibleDate
  }



// 1992-11-18

  return(
    <div>
      <MaskedInput
          mask={[/\d/, /\d/, "/", /\d/, /\d/, "/", /\d/, /\d/, /\d/, /\d/,]}
          className="inputClases"
          placeholder="Día/Mes/año"
          guide={true}
          // name={item.name}
          autoFocus={true}
          pipe={autoCorrectedDatePipe}
          onChange={onChange}
       />

       {/* <input
        type="date"
        onChange={onChange2}
        /> */}
    </div>
  )

}

