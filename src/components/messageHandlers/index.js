import React, { useEffect, useState } from 'react'
import styled from 'styled-components'


const PluginCord = () => {

  const [ handleFound, sethandleFound ] = useState()

  const clickHandle = () => {
    console.log('click')
    if(window['webkit'].messageHandlers['cordova_iab']) {
        const messageObj = {jwt: 'eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNvbnN0cnV5YWNvbCthbmRyZXNAZ21haWwuY29tIiwibGFuZ3VhZ2UiOiJlcyIsImlzcyI6IjVlNzk0NzE3NjRkY2RiMDE2YTM2OWNkOCIsInVzciI6IjVlY2VhYThlY2RmYjc0MDBlMTg3MDY4NiIsImp0aSI6ImRNT2ozOEJXeG1mSmZWdHRGQ1JJSk00NzlrMnpJWVBaVWJVSElFcG9PUkpMZmxCNGRGamQzaEdrT2JRZW1GbzMiLCJhdWQiOiJ0cmFuc2FjdGlvbixhdXRoLGlkZW50aXR5LGluZm8sZGVwb3NpdCxhY2NvdW50LHdpdGhkcmF3LHN3YXAiLCJtZXRhZGF0YSI6IntcImNsaWVudElkXCI6XCI1ZTc5NDcxNzY0ZGNkYjAxNmEzNjljZDhcIixcInRoaXJkX3BhcnR5XCI6XCJ0cnVlXCIsXCJ1aWRcIjpcIjVmMGE0ZWFlNjhlODI5MDBkMWU5MDU1MFwiLFwidG9rZW5cIjpcImJiNmIzODRlNmYxYjU0Y2JkY2M0Zjg5OTg2NzZhMTBkYzU2OTkwZGUxZGRjNWU3NzgyZjMwYWU4Zjc2ZjkzZGUwODFlNTcyZTU5OTdkMDA2N2Y5ZTM4Y2U0NzFkMzhmOWFjMmE2NDg4ZDZjMmJlN2ZmMmI4M2FlMzA3MjUwNjY1XCJ9IiwiaWF0IjoxNTk1NTYwMjQyLCJleHAiOjE1OTU1NzEwNDJ9.SBNHdNOz8vjaQpNd3t3rR1dFg9Awdp2v34tJOB4iie04UyR6s9dxAyWsKLDbaRCMjML9Dvz18Z04YOy9Yvsbug'};
        const stringifiedMessageObj = JSON.stringify(messageObj);
        window.webkit.messageHandlers.cordova_iab.postMessage(stringifiedMessageObj);
        return
      }
  }

  const searchHandle = () => {
    if(window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers['cordova_iab']){
      var messageObj = {my_message: 'MENSAJE DESDE WEB VIEW APP'}
      var stringifiedMessageObj = JSON.stringify(messageObj)
      console.log('EMITING...', stringifiedMessageObj, window.webkit)
      window.webkit.messageHandlers.cordova_iab.postMessage(stringifiedMessageObj);
    }
  }

  // useEffect(()=>{
  //   INTERVAL = setInterval(()=>{
  //     searchHandle()
  //   }, 6000)
  // }, [])

  // consultar messageHandlers
  // probar window open

  return(
    <Container onClick={searchHandle}>
      <p>ÁREA CLICKEABLE</p>
    </Container>
  )

}

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: green;
  display: grid;
  align-items: center;
  justify-items:center;

  p{
    font-size: 35px;
    color: yellow;
  }
`

export default PluginCord
