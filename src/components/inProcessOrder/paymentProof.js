import React from 'react'
import styled from 'styled-components'

const PaymentProofComponent = ({ state }) => {

  return(
    <OverflowContainer>
      <Container className={`${state}`}>
        <ImgContainer>

        </ImgContainer>
        <CropEdit></CropEdit>
      </Container>
    </OverflowContainer>
  )

}

const CropEdit = styled.div`
  width: 100%;
  height: 50px;
  background: #f5f5f5;
  border-radius: 4px;
  box-shadow: 0px 0px 5px 3px rgba(0,0,0,0.1);
`

const ImgContainer = styled.div`
  width: 100%;
  max-width: 550px;
  background: rgba(255, 255, 255, .4);
  border-radius: 6px;
`

const OverflowContainer = styled.section`
  width: 100%;
  height: 100%;
  position: absolute;
  z-index: 3;
  display: grid;
  overflow: hidden;
`

const Container = styled.div`

  padding: 50px;
  background: #232c35;
  transition: .3s;
  transform: translateX(100%);
  display: grid;
  grid-template-rows: 1fr auto 120px;
  row-gap:20px;
  justify-items:center;

  &.activated{
    animation-name: activated;
    animation-duration: .8s;
    animation-fill-mode: forwards;
  }

  @keyframes activated{
    0%{
      transform: translateX(100%);
    }
    50%{
      transform: translateX(100%);
    }
    100%{
      transform: translateX(0%);
    }
  }
`

export default PaymentProofComponent
