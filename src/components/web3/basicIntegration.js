import React, { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components'
import { connector, getLibrary } from '../../config/web3'
import { Web3ReactProvider, useWeb3React } from '@web3-react/core' 

function Web3(props) {

    const { 
        activate, 
        active, 
        deactivate, 
        account,
        chainId,
        error
    } = useWeb3React()

    const connect = useCallback(() => {
        activate(connector)
        localStorage.setItem("prevConnected", 'true')
    }, [activate])

    const disconnect = () => {
        deactivate()
        localStorage.removeItem("prevConnected")
    }

    useEffect(() => {
        if(localStorage.getItem('prevConnected') === 'true') connect();
    }, [connect])

    console.log('Web3', props)

    if(error)return <p>No ha sido posible conectarse a tu wallet</p>

  return (
        <Wrapper>
            <Container>
                {
                    !active ?
                    <>
                        <p>Esperando conexión con billetera</p>
                        <ButtonConnect onClick={connect}>
                            Connect Metamask
                        </ButtonConnect>
                    </>
                    :
                    <>
                        <p>You are connected to {chainId} network</p>
                        <p>Your account is {account}</p>
                        
                        <ButtonConnect onClick={disconnect}>
                            Disconnect
                        </ButtonConnect>

                    </>
                }
            </Container>
        </Wrapper>
    );
}


const Web3Provider = AsComponent => {
    return function (props) {
        return (
            <Web3ReactProvider getLibrary={getLibrary}>
                <AsComponent {...props} />
            </Web3ReactProvider>
        );
    };
}


export default Web3Provider(Web3);


const Container = styled.div`
    width: auto;
    height: auto;
    max-height: 350px;
    display: grid;
    justify-items: center;
`

const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: grid;
  align-items: center;
  justify-items: center;
`

const ButtonConnect = styled.button`
  height: 40px;
  padding: 0 20px;
  max-width: 250px;
`



    // const [ userData, setUserData ] = useState()
    // const metaMaskConnect = async() => {
    //     const ethInstance = window?.ethereum
    //     if(!ethInstance)return alert('Debes instalar una wallet de ethereum');
    //     console.log('ethInstance', ethInstance)
    //     debugger
    //     try {
    //         const res = await ethInstance.request({ method: 'eth_requestAccounts' })
    //         setUserData(res)
    //     }catch (error) {
    //         console.log('ConnectError', error)
    //     }
    // }
    // const connect = (walletName) => {
    //     const WALLET_METHODS = {
    //         metaMask:metaMaskConnect
    //     }
    //     return WALLET_METHODS[walletName]()
    // }