import { Web3OnboardProvider, init } from '@web3-onboard/react'
import injectedModule from '@web3-onboard/injected-wallets'
import { CHAINS, APP_METADATA } from '../../config/web3/const'
import uauthBNCModule from '@uauth/web3-onboard'
import UAuth from '@uauth/js'

const CLIENT_ID = "87c7ec9d-784f-421d-b012-062e218444a2"
const REDIRECT_URI = "http://localhost:3000"

export const uauth = new UAuth({
    clientID: CLIENT_ID,
    redirectUri: REDIRECT_URI,
    postLogoutRedirectUri: REDIRECT_URI,
    // clientSecret:"epaaaa",
    // Scope must include openid and wallet
    scope: 'openid wallet'
    // Injected and walletconnect connectors are required.
    // connectors: {connector, walletconnect},
  })
  
  const uauthBNCOptions = {
      uauth: uauth
  }
  
  const uauthModule = uauthBNCModule(uauthBNCOptions)

const chains = CHAINS
const appMetadata = APP_METADATA
const wallets = [ injectedModule(), uauthModule ]
 
const web3Onboard = init({
  wallets,
  chains,
  appMetadata
})

const Web3Provider = AsComponent => {
    return function (props) {
        return (
            <Web3OnboardProvider web3Onboard={web3Onboard}>
                <AsComponent {...props} />
            </Web3OnboardProvider>
        );
    };
}

export default Web3Provider
