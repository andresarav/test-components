import { useEffect, useState } from 'react'
import { useConnectWallet } from '@web3-onboard/react'
import { ethers } from 'ethers'
import Web3Provider from './web3Provider'

function ConnectWallet() {

  const [ { wallet, connecting }, connect, disconnect ] = useConnectWallet()
  const [ ethersProvider, setProvider ] = useState()

  const walletConnect = async() => {
    const walletsConnected = await connect()
    console.log('connected wallets: ', walletsConnected)
  }

  useEffect(() => {
    // If the wallet has a provider than the wallet is connected
    if (wallet?.provider) {
      setProvider(new ethers.providers.Web3Provider(wallet.provider, 'any'))
    }
  }, [wallet])

  return (
    <div>
      <button
        disabled={connecting}
        onClick={walletConnect}
        >
          Connect
      </button>
    </div>
  )
}


export default Web3Provider(ConnectWallet)
