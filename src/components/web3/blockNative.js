import React, { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components'
import { onboard, getLibrary } from '../../config/web3'
import { ethers } from 'ethers'
// import { Web3ReactProvider } from '@web3-react/core' 
import SVG from 'react-inlinesvg';

const WALLET_TYPE = { accounts:[], chains:[], icon:undefined, label:undefined, provider:undefined }

function Web3(props) {

    const [{ accounts, chains, icon, label, provider }, setWalletState ] = useState(WALLET_TYPE)

    const connectWallet = useCallback(async() => {
        try {
            const wallets = await onboard.connectWallet()
            const { wallets:[ primaryWallet ], ...onBoardArgs } = await onboard.state.get()
            if (!primaryWallet) throw "No ha sido posible consultar la cuenta";
            setWalletState(primaryWallet)

            // create an ethers provider with the last connected wallet provider
            const ethersProvider = new ethers.providers.Web3Provider(wallets[0].provider, 'any')
            const signer = ethersProvider.getSigner()

            console.log('ethersProvider', ethersProvider)
            debugger
            
            // // send a transaction with the ethers provider
            // const txn = await signer.sendTransaction({
            //     to: '0x',
            //     value: 100000000000000
            // })
            
            // const receipt = await txn.wait()
            // console.log(receipt)

        } catch (error) {
            console.error(error);
            debugger
        }
        // const image = wallets[0]?.icon;
        // const buff = new ArrayBuffer(image);
        // const base64data = buff.toString('base64');
        // console.log('image ==> ', base64data)
        // debugger
    }, [])

    // const disconnect = async () => {
    //     // const [primaryWallet] = await onboard.state.get().wallets;
    //     // if (primaryWallet) await onboard.disconnectWallet({ label: primaryWallet.label });
    //     // refreshState();
    // };

    const handleAction = () => {
        console.log('provider', provider, props)
    }

    console.log(accounts, chains, label, provider)

    useEffect(() => {
        // console.log('OnBoard', onboard)
        // if(localStorage.getItem('prevConnected') === 'true') connect();
    })

  let error
  let active


  return(
        <Wrapper>
            <Container>

                {
                    icon && <SVG width={30} src={icon} />
                }

                {
                    !accounts[0] ?
                    <>
                        {error && <p style={{ color:"red" }}>No ha sido posible conectarse a esta mierda</p>}
                        <p>Esperando conexión con billetera</p>
                        <ButtonConnect onClick={connectWallet}>
                            Connect Wallet
                        </ButtonConnect>
                    </>
                    :
                    <>
                        <p>{label}</p>
                        <p>Your account is {accounts[0]?.address}</p>
                        <p>Network:{provider?.networkVersion}({provider?.chainId})</p>
                        
                        {/* <ButtonConnect onClick={disconnect}>Disconnect</ButtonConnect> */}
                        <ButtonConnect onClick={connectWallet}>
                            Connect Wallet
                        </ButtonConnect>
                        {
                            provider &&
                            <ButtonConnect onClick={handleAction}>
                                HandleProvider
                            </ButtonConnect>
                        }

                    </>
                }
            </Container>
        </Wrapper>
    );
}


// const Web3Provider = AsComponent => {
//     return function (props) {
//         return (
//             <Web3ReactProvider getLibrary={getLibrary}>
//                 <AsComponent {...props} />
//             </Web3ReactProvider>
//         );
//     };
// }
// export default Web3Provider(Web3);
export default Web3;


const Container = styled.div`
    width: auto;
    height: auto;
    max-height: 350px;
    display: grid;
    justify-items: center;
`

const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: grid;
  align-items: center;
  justify-items: center;
`

const ButtonConnect = styled.button`
  height: 40px;
  padding: 0 20px;
  max-width: 250px;
`


