import React from 'react'

export default () => {

  const data = {
    frontend: 'Juan',
    backend: 'Carlos',
    design: 'Ana'
}
  const entries = Object.entries(data)
  const key = Object.keys(data)
  const values = Object.values(data)

  console.log(entries)
  console.log(key)
  console.log(values);

  return null
}
