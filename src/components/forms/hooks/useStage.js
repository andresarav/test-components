import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";
import { getQuery } from '../utils'

const useStage = (stageController, stages, defaultStage = 9) => {

  const history = useHistory()
  const [ stageStatus, setStageStatus ] = useState()
  const [ currentStage, setCurrentStage ] = useState(defaultStage)
  const [ stageData, setStageData ] = useState(stages[stageController[currentStage]])

  const nextStage = () => {
    // add ref or className on Json file Source to management the DOM components, e.x buttons states...
    if(currentStage >= stageController.length){return}
      setCurrentStage(prevState => prevState+1)
  }

  const prevStage = () => {
    if(currentStage <= 0){return}
    setCurrentStage(prevState => prevState-1)
  }

// Set url query params
  useEffect(()=>{
    if(stageData?.settings?.queryParams){
      let query = getQuery(stageData.settings.queryParams)
      history.push(query)
    }
  }, [stageData])

  useEffect(()=>{
    const nextStageData = stages[stageController[currentStage]] 
    setStageData(nextStageData)
  }, [stages[stageController[currentStage]]])

  return {
    currentStage,
    nextStage,
    prevStage,
    stageController,
    stageData,
    setStageData,
    stageStatus,
    setStageStatus,
    finalStage:currentStage >= stageController.length
  }

}

export default useStage
