import React, { useEffect, useState, Suspense } from 'react'

const DynamicLoadComponent = ({ component, Fallback, ...props }) => {

 const [ RenderComponent, setRenderComponent ] = useState()

 const initialize = async() => {
   if(!component){return}
   const loadedComponent = await import(`./components/${component}`)
     setRenderComponent(loadedComponent)
 }

  useEffect(()=>{
      initialize()
  }, [component])

  if(Fallback && !RenderComponent){
    return (<Fallback/>)
  }

  return(
    <>
      { RenderComponent && <RenderComponent.default {...props} /> }
    </>
  )
}

export default DynamicLoadComponent
