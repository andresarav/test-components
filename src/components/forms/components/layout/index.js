import { useEffect } from 'react'
import { Wrapper } from './styles'
import Portal from '../portal'

const Layout = ({ children }) => {

  return(
    <Wrapper id="mainLayout">
      {children}
    </Wrapper>
  )
}

export default Layout
