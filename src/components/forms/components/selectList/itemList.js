import React, { useEffect } from 'react'
import { ItemListGrid } from './styles'
import { useObserver } from '../../hooks/intersectionObserver'


const ItemList = ({ item, onClick }) => {

  const [ show, element ] = useObserver()

  const imgSrc = item?.img || item?.flag

  return(
    <ItemListGrid className="itemListGrid" onClick={onClick} ref={element}>
      <div className="itemList__icon" >
      {
          imgSrc &&
           <img src={show ? imgSrc : ''} width={35} height={35}></img>
      }
      </div>
      <p className="countryName" >{item?.uiName}</p>
    </ItemListGrid>
  )
}

export default React.memo(ItemList)





