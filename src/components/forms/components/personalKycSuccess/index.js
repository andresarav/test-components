import React, { useEffect } from "react"
// import ConfettiComponent from '../success/confetti'
import { Medal } from '../success/icons'
import { LayoutContainer, ControlContainer } from '../success/styles'
import styled from "styled-components"

const PersonalKycSuccess = ({ handleDataForm:{ dataForm, setDataForm }, handleState:{ state, setState } }) => {

    console.log('|||||| PersonalKycSuccess  ==> ', dataForm)

    return(
        <LayoutContainer>
            {/* <ConfettiComponent/> */}
            <h1>Genial {state?.name?.toLowerCase()}</h1>
            <Medal size={150} />
            <p>Has completado tu verificación básica</p>
            <ControlContainer>
                <p>¿Deseas continuar con el proceso de verificación avanzada?</p>
            </ControlContainer>
        </LayoutContainer>
    )
}

export default PersonalKycSuccess


