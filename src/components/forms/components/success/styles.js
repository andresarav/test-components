import styled from "styled-components"

export const LayoutContainer = styled.section`
    display: grid;
    width: calc(100vw - 4em);
    height: calc(100vh - 4em);
    grid-template-rows: auto auto auto 1fr;
    justify-items: center;
    padding: 2em;

    h1{
        margin-bottom:10vh;
    }

    &>p{
        margin-top:10vh;
        font-size:20px;
        max-width: 500px;
        text-align: center;
    }
`

export const ControlContainer = styled.div`

`