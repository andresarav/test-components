import styled, { keyframes } from "styled-components";
import React from "react";
import KycSkeleton from './skeleton'

const SendingView = props => {

    return(
        <>
            <SendingViewContainer className="send"/>
            <KycSkeleton/>
        </>
    )

}

export default SendingView


const entry = keyframes`
  0% {
    transform: translateY(100vh);
  }
  100% {
    transform: translateY(0vh);
  }
`;

const SendingViewContainer = styled.div`
  position:absolute;
  top:0;
  left:0;
  width: 100vw;
  height: 100vh;
  z-index:4;
  background: rgb(255 255 255 / 64%);
  transform: translateY(100vh);
  animation: ${entry} 1s linear forwards;

`