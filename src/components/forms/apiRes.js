const countryValidators = {
    res:{
        levels:{
            level_1:{
                identity:{},
                personal:{
                    natural:{
                        name:{
                            ui_name: "Nombres completos"
                        },
                        phone:{
                            ui_name: "Numero de teléfono",
                            ui_type: "phone"
                        },
                        nationality:{
                            colombia:{
                                ui_name: "Colombia"
                            },
                            afghanistan:{
                                ui_name: "Afghanistan"
                            },
                            ui_name: "Nacionalidad del documento",
                            ui_type: "select"
                        },
                        address:{
                            ui_name:"Dirección de residencia"
                        },
                        city:{
                            ui_name: "Ciudad"
                        },
                        id_type:{
                            ui_name: "Tipo de documento",
                            ui_type: "select",
                            cedula_ciudadania:{
                                ui_name: "Cedula de ciudadanía"
                            },
                            cedula_extranjeria:{
                                ui_name: "Cedula de extranjería"
                            },
                            pasaporte:{
                                ui_name: "Pasaporte"
                            },
                            pep:{
                                ui_name: "Permiso especial de permanencia"
                            }
                        },                        
                        id_number:{
                            ui_name: "Número de documento",
                            ui_type: "text"
                        },
                        birthday:{
                            ui_name: "Fecha de nacimiento",
                            ui_type: "date"
                        },
                        country:{
                            colombia:{
                                ui_name: "Colombia",
                                value: "colombia"
                            },
                            peru:{
                                ui_name: "Perú",
                                value: "peru"
                            },
                            ui_name: "País",
                            ui_type: "select"
                        },
                        surname:{
                            ui_name: "Apellidos completos"
                        }
                    }
                        
                }
            }
        }
    }
}

export default countryValidators