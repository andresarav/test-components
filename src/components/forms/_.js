

let stages = {
  "id_type":{
    uiName:"Tipo de documento:",
    key:"id_type",
    uiType:"text",
    "settings":{
      defaultMessage:"Elige el documento de identidad con el cual verificarás tu cuenta.",
      successPattern:/[a-zA-Z ]{3,40}/g,
      label:"Tipo de documento:",
      placeholder:"Elije tu documento de identidad",
      queryParams:{
        form:'personal_id_type'
      },
      errors:[
        // { pattern:/[^a-zA-Z ]{1,30}/g, message:'Solo se permiten letras...'}
      ],
    }
  },
  "city":{
    uiName:"Ciudad de residencia",
    key:"city",
    uiType:"text",
    "settings":{
      defaultMessage:"Escribe la ciudad en la que resides actualmente",
      successPattern:/[a-zA-Z ]{3,40}/g,
      label:"Ciudad de residencia:",
      placeholder:"Elije tu ciudad actual",
      queryParams:{
        form:'personal_residence_city'
      },
      errors:[
        { pattern:/[^a-zA-Z ]{1,30}/g, message:'Solo se permiten letras...'}
      ],
    }
  },
  "address":{
    uiName:"Dirección de residencia:",
    key:"address",
    uiType:"text",
    "settings":{
      defaultMessage:"Escribe de forma completa tu dirección actual de residencia",
      successPattern:/[a-zA-Z ]{3,40}/g,
      label:"Dirección de residencia:",
      placeholder:"Escribe la dirección",
      queryParams:{
        form:'personal_address'
      },
      errors:[
        { pattern:/[^a-zA-Z ]{1,30}/g, message:'Solo se permiten letras...'}
      ],
    }
  },
  "surname":{
    uiName:"Apellidos completos",
    key:"surname",
    uiType:"text",
    "settings":{
      defaultMessage:"Tus apellidos deben coincidir con los de tu documento de identidad.",
      successPattern:/[a-zA-Z ]{3,40}/g,
      label:"Apellidos completos:",
      placeholder:"Ej: Sanchez buitrago",
      queryParams:{
        form:'personal_surnames'
      },
      errors:[
        { pattern:/[^a-zA-Z ]{1,30}/g, message:'Solo se permiten letras...'}
      ],
    }
  },
  "name":{
    uiName:"Nombres completos",
    key:"name",
    uiType:"text",
    "settings":{
      defaultMessage:"Los nombres deben coincidir con los de tu documento de identidad",
      successPattern:/[a-zA-Z ]{3,40}/g,
      label:"Nombres completos (sin apellidos):",
      placeholder:"Ej: Juan josé ",
      queryParams:{
        form:'personal_names'
      },
      errors:[
        { pattern:/[^a-zA-Z ]{1,30}/g, message:'Solo se permiten letras...'}
      ],
      AuxComponent:null,
      MainComponent:null
    }
  },
  "nationality":{
    uiName:"Nacionalidad del documento:",
    key:"nationality",
    uiType:"select",
    "settings":{
      defaultMessage:"Selecciona la nacionalidad de tu documento de identidad",
      successPattern:/[a-zA-Z _]{1,40}/g,
      errors:[
        { pattern:/[^a-zA-Z _]{1,30}/g, message:'Solo se permiten letras...'}
      ],
      label:"Nacionalidad del documento:",
      placeholder:"Ej: pasaporte",
      queryParams:{
        form:'personal_country'
      }
    }
  },
  "country":{
    uiName:"País",
    key:"country",
    uiType:"select",
    selectList:{
      "colombia":{
        uiName:"Colombia",
        value:"colombia",
        prefix:"+57",
        img:"https://upload.wikimedia.org/wikipedia/commons/2/21/Flag_of_Colombia.svg"
      },
      "peru":{
        uiName:"Perú",
        value:"peru",
        prefix:"+51",
        img:"https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/Flag_of_Peru_%28state%29.svg/800px-Flag_of_Peru_%28state%29.svg.png"
      },
      "chile":{
        uiName:"Chile",
        value:"chile",
        prefix:"+56",
        img:"https://upload.wikimedia.org/wikipedia/commons/7/78/Flag_of_Chile.svg"
      },
      "republica_dominicana":{
        uiName:"República Dominicana",
        value:"republica_dominicana",
        prefix:"+1",
        img:"https://upload.wikimedia.org/wikipedia/commons/9/9f/Flag_of_the_Dominican_Republic.svg"
      }
    },
    "settings":{
      defaultMessage:"Elige el país desde el que operarás tu cuenta",
      successPattern:/[a-zA-Z _]{1,40}/g,
      errors:[
        { pattern:/[^a-zA-Z _]{1,30}/g, message:'Solo se permiten letras...'}
      ],
      label:"País de operación:",
      placeholder:"Ej: Juan josé ",
      queryParams:{
        form:'personal_country'
      },
      auxComponent:null,
      mainComponent:null
    }
  },
  "birthday":{
    uiName:"Fecha de nacimiento",
    key:"birthday",
    uiType:"date",
    "settings":{
      defaultMessage:"Ingresa tú fecha de nacimiento (Día / Mes / Año)",
      props:{
        min:"1930-01-01",
        max:"2003-12-31",
        pattern:"[0-9]{2}-[0-9]{2}-[0-9]{4}"
      },
      queryParams:{
        form:'personal_birthday'
      },
      // errors:[
      //   { pattern:/1[0-7]/g, message:'Debes de ser mayor de edad(+17) para continuar'}
      // ],
    }
  },
  "id_number":{
    uiName:"Número de documento",
    key:"id_number",
    uiType:"text",
    "settings":{
      defaultMessage:"Digíta tu número de documento",
      successPattern:{
       pasaporte:/[0-9A-Z]{5,15}/g,
       others:/[0-9]{5,15}/g
      },
      label:"Número de documento",
      placeholder:"Dígita el número del documento que elegíste",
      queryParams:{
        form:'personal_number_id'
      },
      errors:{
        pasaporte:[ 
          { pattern:/[^0-9A-Z]/g, message:'Solo se permiten valores alfanuméricos...' }
        ],
        others:[
          { pattern:/[^0-9]/g, message:'Solo se permiten valores númericos...' }
        ]
      },
      mainComponent:null
    }
  },
  "phone":{
    uiName:"Número de celular",
    key:"phone",
    uiType:"text",
    "settings":{
      defaultMessage:"Digíta tu número de celular",
      successPattern:/[0-9]{5,40}/g,
      label:"Número de celular:",
      queryParams:{
        form:'personal_phone'
      },
      errors:[
        { pattern:/[^0-9]/g, message:'Solo se permiten Números...'}
      ],
      auxComponent:"countryPrefix", //targetId to render component
      mainComponent:null
    }
  }
} 

const defaultState = {
  address: "cra 22",
  birthday: "2003-12-12",
  city: "Calis",
  country: "colombia",
  id_number: "2323323332",
  id_type: "cedula_extranjeria",
  meta_phone: "colombia",
  name: "Andres Felipe",
  nationality: "colombia",
  phone: "3178889898",
  surname: "guevara garcia"
}

const handleError = {
  identity:{},
  financial:{},
  personal:{
    defaultErrorMessage:"Tu verificación ha sido rechazada, corríge los campos indicados.",
    // errors:{
    //   country:"Ingresa un país de operación válido...",
    //   name:"Solo ingresa nombres sin apellidos..."
    // }
  }
}

const successStage = {
  component:"personalKycSuccess"
}

const formStructure = {
  wrapperComponent:"personalKycComponent",
  handleError:handleError.personal,
  successStage,
  defaultState,
  stages
}


export default formStructure
