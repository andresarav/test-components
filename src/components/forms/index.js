import React, { useState, useEffect } from 'react'
import DynamicLoadComponent from './dynamicLoadComponent'
import KycSkeleton from './components/personalKycComponent/skeleton'
import { getInitialState } from './utils'
import './mobile.css'
import './global.css'

const FormComponent = ({ handleDataForm }) => {

  const [ state, setState ] = useState()

  useEffect(()=>{
    setState(getInitialState(handleDataForm.dataForm))
  }, [handleDataForm.dataForm.wrapperComponent])
  
  return( 
        <DynamicLoadComponent
          component={handleDataForm.dataForm.wrapperComponent}
          Fallback={() => <KycSkeleton/>}
          handleDataForm={handleDataForm}
          handleState={{setState, state}}
        />
  )
}

export default FormComponent
