import React from 'react'

class ChildrenRef extends React.Component {

  state = {
    childrenState:1,
    childrenState2:2,
  }

  handleActionChild = () => {
    console.log('handleActionChild')
  }

  render(){
    return(
      <div>
        ChildrenRef
      </div>
    )
  }

}


export default ChildrenRef
