import React from 'react'

const FunctionalChildren = React.forwardRef((props, ref) => {

  const clickeame = () => {}

  return(
    <button ref={ref} className="FancyButton">
      {props.children}
    </button>
  )
});


export default FunctionalChildren
