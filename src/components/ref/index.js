import React, { useRef, useEffect } from 'react'
import ChildrenRef from './children'
import FunctionalChildren from './functionalChildren'


export default () => {

  const inputRef = useRef()

  useEffect(()=>{
    console.log(inputRef.current)
    inputRef.current.handleActionChild()
  }, [])

  return(
    <ChildrenRef ref={inputRef}/>
    // <FunctionalChildren ref={inputRef} />
  )
}
