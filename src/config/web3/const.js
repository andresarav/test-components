const INFURA_ID = "7d741d1857b244669b740c0411b2fc55"
const INFURA_URI = `https://mainnet.infura.io/v3/${INFURA_ID}`
const RPC_URL_POLYGON = 'https://matic-mainnet.chainstacklabs.com'
import { IMG_STRING } from './logo'

export const CHAINS = [
    {
      id: '0x1',
      token: 'ETH',
      label: 'Ethereum Mainnet',
      rpcUrl: INFURA_URI
    },
    {
        id: '0x89',
        token: 'MATIC',
        label: 'Polygon Mainnet',
        rpcUrl: RPC_URL_POLYGON
    }
]


export const APP_METADATA = {
    name: 'Coinsenda',
    icon: IMG_STRING,
    logo: IMG_STRING,
    gettingStartedGuide:"https://coinsenda.com/docs/terms",
    description: 'Coinsenda Dex',
    recommendedInjectedWallets: [
        { name: "Coinbase", url: "https://wallet.coinbase.com/" },
        { name: "MetaMask", url: "https://metamask.io" }
    ]
}