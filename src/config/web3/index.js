import Onboard from '@web3-onboard/core'
import web3 from "web3"
// import {InjectedConnector} from '@web3-react/injected-connector'
// import {WalletConnectConnector} from '@web3-react/walletconnect-connector'
// import {UAuthConnector} from '@uauth/web3-react'
import uauthBNCModule from '@uauth/web3-onboard'
import injectedModule from '@web3-onboard/injected-wallets'
import UAuth from '@uauth/js'
import { CHAINS } from './const'
// import walletConnectModule from "@web3-onboard/walletconnect";
// const ETHEREUM_NETWORK_ID = 1;

const CLIENT_ID = "87c7ec9d-784f-421d-b012-062e218444a2"
const REDIRECT_URI = "http://localhost:3000"
import { IMG_STRING } from './logo'

export const getLibrary = (provider) => {
    const library = new web3(provider)
    return library
}

// Instanciate your other connectors.
// export const connector = new InjectedConnector({supportedChainIds: [ETHEREUM_NETWORK_ID]})
// export const walletconnect = new WalletConnectConnector({
//   infuraId: INFURA_ID,
//   qrcode: true,
// })

export const uauth = new UAuth({
  clientID: CLIENT_ID,
  redirectUri: REDIRECT_URI,
  postLogoutRedirectUri: REDIRECT_URI,
  // clientSecret:"epaaaa",
  // Scope must include openid and wallet
  scope: 'openid wallet'
  // Injected and walletconnect connectors are required.
  // connectors: {connector, walletconnect},
})

const uauthBNCOptions = {
    uauth: uauth
}

const uauthModule = uauthBNCModule(uauthBNCOptions)
const injected = injectedModule()
// const walletConnect = walletConnectModule();

export const onboard = Onboard({
    wallets: [injected, uauthModule],
    chains: CHAINS,
    appMetadata: {
        name: 'Coinsenda',
        icon: IMG_STRING,
        logo: IMG_STRING,
        gettingStartedGuide:"https://coinsenda.com/docs/terms",
        description: 'Coinsenda Dex',
        recommendedInjectedWallets: [
            { name: "Coinbase", url: "https://wallet.coinbase.com/" },
            { name: "MetaMask", url: "https://metamask.io" }
        ]
    }
})


