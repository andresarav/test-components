import React, { useState, useEffect, useRef } from 'react'
import styled from 'styled-components'
// import RefContainer from './components/ref'
// import MyDropzone from './components/dropZone/dropZone'
// import InProcessOrder from './components/inProcessOrder/inProcessOrder'
// import ZoomComponent from './components/zoomComponent/zoom'
// import InifiniteScroll from './components/infiniteScroll'
// import ItemWalet from './components/itemCurrencyWallet'
// import SelectComponent from './components/select'
// import Es from './components/ES/es8'
// import GetBrowserInfo from './components/getBrowserInfo'
// import Lodash from './components/lodash'
// import { Router, Switch, Route } from 'react-router-dom'
// import { createBrowserHistory } from "history"
// import Streaming from './components/streamingVideo'
// import ImageTypeComponent from './components/imageType'
// import SelectFilter from './components/selectList'
// import DateComponent from './components/date'
// import Web3 from './components/web3'
// import Web3BNOnboard from './components/web3/blockNative'
import Web3App from './components/web3'


const App = props => {
  return(
    <Web3App/>
  )
}

export default App

const TestGridComponent = props => {

  return(
    <Layout>
      <Item1></Item1>
      <Item2></Item2>
    </Layout>
  )

}

const Item = styled.div`
  height:250px;
  border:1px solid red;
`

const Item1 = styled(Item)`
  background:green;
  min-width:750px;
  width:auto;
`

const Item2 = styled(Item)`
  background:yellow;
  min-width:350px;
  width:auto;
`

const Layout = styled.div`
    display: grid;
    column-gap: 15px;
    height: auto;
    display: grid;
    column-gap: 15px;
    grid-auto-columns: minmax(10px,auto);
    grid-template-columns: repeat(auto-fit,minmax(400px,auto));
    min-height: 400px;
    row-gap: 15px;
`