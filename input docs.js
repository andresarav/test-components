const AuxComponentContainer = ({ AuxComponent }) => {
    typeof AuxComponent === "function" ? (
      <AuxComponent />
    ) : (
      typeof AuxComponent === "object" &&
      AuxComponent.map((SingleAuxComponent, idItem) => {
        return <SingleAuxComponent key={idItem} />;
      })
    );

{AuxComponent && <AuxComponentContainer AuxComponent={AuxComponent} />}
